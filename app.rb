require 'rubygems'
require 'bundler'

Bundler.require
require_relative './routes/posts.rb'
require_relative './models/post.rb'
require_relative './models/user.rb'

set :bind, '0.0.0.0'
set :sessions, true
set :user, {name: "public", password: ""}

configure do
	MongoMapper.setup( {'production' => { 'uri' => ENV['DATABASE_URI'] } }, 'production')
end

get '/' do
	"Hello, #{settings.user[:name]}"
end

get '/assets/style' do
	scss :style
end