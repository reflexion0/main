class User
	include MongoMapper::Document

	key :name, String
	key :user, String
	key :digest, String
	key :rights, String
end