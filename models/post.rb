class Post
	include MongoMapper::Document

	key :slug, String
	key :title, String
	key :author, String
	key :time, Time
	timestamps!
end