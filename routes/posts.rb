get '/posts' do
	redirect '/posts/look/all'
end

get '/posts/look/all' do
	@posts = Post.all()
	haml :allposts
end

get '/posts/look/:slug' do
	@post = Post.all(slug: params[:slug])[0]
	haml :post
end

get '/posts/create' do
	haml :newpost
end

get '/posts/:slug/edit' do
	@post = Post.all(slug: params[:slug])[0]
	haml :editpost
end

# POST Requests
post '/posts/:slug' do
	if params[:action] == "edit" then
		redirect "/posts/#{params[:slug]}/edit"
	elsif params[:action] == "delete"
		Post.destroy_all(slug: params[:slug])
		redirect '/posts/look/all'
	end	
end

post '/posts/create' do
	puts params
	Post.create({
		slug: params[:slug],
		title: params[:title],
		author: params[:author],
		code: params[:code],
		time: Time.now
	})
	redirect '/posts/look/#{params[:slug]}'
end

post '/posts/delete' do
	Post.destroy_all(slug: params[:slug])
end

post '/posts/delete/all' do
	Post.destroy_all()
end